//
//  Event.swift
//  Hobbe
//
//  Created by Andres Donoso on 8/10/20.
//

import Foundation
import CoreLocation

typealias Events = [Event]

class EventList {
	private var events: [Event]
	
	init(events: [Event] = []) {
		self.events = events
	}
	
	func getEvents() -> [Event] {
		return events
	}
	
	func setEvents(events: [Event]) {
		self.events = events
	}
}

class Event: Activity {
	private var category: String
	private var cost: String
	private var organizer: String
	
	enum EventCodingKeys: String, CodingKey {
		case category = "Category"
		case cost = "Cost"
		case organizer = "Organizer"
	}
	
    init(id: Int = 0, name: String = "", description: String = "", location: String = "", address: String = "", date: Date = Date(), hour: String = "", image: String = "", temperature : Double = 0, category: String = "", cost: String = "", organizer: String = "", lat:Double = 0, lon:Double = 0, users: [User] = []) {
		self.category = category
		self.cost = cost
		self.organizer = organizer
		
        super.init(id: id, name: name, description: description, location: location, address: address, date: date, hour: hour, image: image, temperature: temperature,lat: lat, lon:lon, users: users)
	}
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: EventCodingKeys.self)
		
		category = try container.decode(String.self, forKey: .category)
		cost = try container.decode(String.self, forKey: .cost)
		organizer = try container.decode(String.self, forKey: .organizer)
		
		try super.init(from: decoder)
	}
    
    func getCost() -> String {
        return self.cost
    }
    
    func getOrganizer() -> String {
        return self.organizer
    }
    
    func getCategory() -> String {
        return self.category
    }
    
    
}
