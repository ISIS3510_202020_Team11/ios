//
//  Community.swift
//  Hobbe
//
//  Created by Andres Donoso on 8/10/20.
//

import Foundation

struct Community {
    private var name: String
    private var description: String
    private var type: String
    private var posts: [Post]
    private var image: String
    
    init(name: String, description: String, type: String, posts:[Post], image:String) {
        self.name = name
        self.description = description
        self.type = type
        self.posts=posts
        self.image = image
    }
    
    func getName() -> String {
    return self.name
    }
    
    func getDescription() -> String {
        return self.description
    }
    
    func getType() -> String {
    return self.type
    }
    
    func getPosts() -> [Post] {
    return self.posts
    }
    
    func getImage() -> String {
    return self.image
    }
}
