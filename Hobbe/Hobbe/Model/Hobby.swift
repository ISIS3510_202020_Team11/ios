//
//  Hobby.swift
//  Hobbe
//
//  Created by Andres Donoso on 8/10/20.
//

import Foundation

typealias Hobbies = [Hobby]

struct Hobby: Decodable {
	private var id: Int
    private var name: String
    private var image: String
    private var activities: [Activity]
    private var communities: [Community]
    private var users: [User]
	
	private enum CodingKeys: String, CodingKey {
		case id = "id"
		case name = "name"
	}
	
	init(id: Int = -1, name: String = "", image: String = "", activities: [Activity] = [], communities: [Community] = [], users: [User] = []) {
		self.id = id
		self.name = name
		self.image = image
		self.activities = activities
		self.communities = communities
		self.users = users
	}
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		id = try container.decode(Int.self, forKey: .id)
		name = try container.decode(String.self, forKey: .name)
		
		self.image = ""
		self.activities = []
		self.communities = []
		self.users = []
	}
	
	//MARK: - Getters and Setters
	func getId() -> Int {
		return id
	}
	
	mutating func setId(_ id: Int) {
		self.id = id
	}
	
	func getName() -> String {
		return name
	}
	
	mutating func setName(_ name: String) {
		self.name = name
	}
}
