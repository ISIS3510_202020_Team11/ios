//
//  Group.swift
//  Hobbe
//
//  Created by Andres Donoso on 8/10/20.
//

import Foundation

struct Group {
    private var name: String
    private var description: String
    private var image: String
    private var users: [User]
    private var messages: [Post]
    private var activities: [GroupActivity]
}
