//
//  Post.swift
//  Hobbe
//
//  Created by Andres Donoso on 8/10/20.
//

import Foundation

private enum Type {
    case message
    case communityPost
}

struct Post {
    private var name: String
    private var type: Type
    private var datePublished: Date
    private var content: Content
}
