//
//  Weather.swift
//  Hobbe
//
//  Created by Norberto Duarte M on 17/10/20.
//

import Foundation

struct Weather: Codable {
    let main: Main
}

struct Main: Codable {
    let temp: Double
}
