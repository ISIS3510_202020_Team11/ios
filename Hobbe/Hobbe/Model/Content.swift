//
//  Content.swift
//  Hobbe
//
//  Created by Andres Donoso on 8/10/20.
//

import Foundation
import CoreLocation

struct Content {
    private var text: String?
    private var image: String?
    private var event: Activity?
    private var location: CLLocation?
}
