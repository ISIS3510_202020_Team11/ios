//
//  Activity.swift
//  Hobbe
//
//  Created by Andres Donoso on 8/10/20.
//

import Foundation
import CoreLocation

class Activity: Decodable {
	var id: Int
	var name: String
	var description: String
	var location: String
	var address: String
	var date: Date?
	var hour: String
	var image: String
	var temperature: Double
    var lat: Double
    var lon: Double
	var users: [User]?
	
	private enum ActivityCodingKeys: String, CodingKey {
		case id
		case name = "Name"
		case description = "Description"
		case location = "Location"
		case address = "Address"
		case date = "Date"
		case hour = "Hour"
		case image = "Image"
		case temperature = "Weather "
        case temperatureFB = "Weather"
        case lat = "lat"
        case lon = "long"
	}
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: ActivityCodingKeys.self)
		
		id = try container.decode(Int.self, forKey: .id)
		name = try container.decode(String.self, forKey: .name)
		description = try container.decode(String.self, forKey: .description)
		location = try container.decode(String.self, forKey: .location)
		address = try container.decode(String.self, forKey: .address)
		//date = Date()
        let dateString = try container.decode(String.self, forKey: .date)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        date = dateFormatter.date(from: dateString)
		hour = try container.decode(String.self, forKey: .hour)
		//image = try container.decode(String.self, forKey: .image)
        image = ""
        do {
            temperature = try container.decode(Double.self, forKey: .temperature)
        } catch {
            temperature = try container.decode(Double.self, forKey: .temperatureFB)
        }
        lat = try container.decode(Double.self, forKey: .lat)
        lon = try container.decode(Double.self, forKey: .lon)
	}
	
    init(id: Int, name: String, description: String, location: String, address: String, date: Date, hour: String, image: String, temperature: Double, lat: Double,lon: Double , users: [User]) {
		self.id = id
		self.name = name
		self.description = description
		self.location = location
		self.address = address
		self.date = date
		self.hour = hour
		self.image = image
		self.temperature = temperature
        self.lat = lat
        self.lon = lon
		self.users = users
	}
	
	func getName() -> String {
		return self.name
	}
	
	func getDescription() -> String {
		return self.description
	}
	
	func getLocation() -> String {
		return self.location
	}
	
	func getAddress() -> String {
		return self.address
	}
	
	func getDate() -> Date {
		return self.date!
	}
    func getHour() -> String {
        return self.hour
    }
	
	func getTemperature() -> Double {
		return self.temperature
	}
    
    func getLat() -> Double {
        return self.lat
    }
    
    func getLon() -> Double {
        return self.lon
    }
	
	func getUsers() -> [User] {
		return self.users!
	}
	
	func getImage() -> String {
		return self.image
	}
	
	func setImage(_ image: String) {
		self.image = image
	}
}
