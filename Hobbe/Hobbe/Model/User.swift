//
//  User.swift
//  Hobbe
//
//  Created by Andres Donoso on 8/10/20.
//

import Foundation

/**
 A user that is part of the application.
*/
struct User {
	
	//MARK: - Properties
    private var name: String
    private var lastname: String
    private var email: String
    private var photo: String
	private var birthDate: Date
    private var groups: [Group]
    private var activities: [Activity]
    private var events: [Event]
    private var communities: [Community]
    private var hobbies: [Hobby]
	
	//MARK: - Computed Properties
	var formattedBirthDate: String {
		let formatter = DateFormatter()
		formatter.dateFormat = "d MMM y"
		return formatter.string(from: birthDate)
	}
	
	//MARK: - Initializers
	init() {
		name = ""
		lastname = ""
		email = ""
		photo = ""
		birthDate = Date()
		groups = []
		activities = []
		events = []
		communities = []
		hobbies = []
	}
	
	//MARK: - Getters and setters
	func getName() -> String {
		return name
	}
	
	mutating func setName(_ name: String) {
		self.name = name
	}
	
	func getLastname() -> String {
		return lastname
	}
	
	mutating func setLastname(_ lastname: String) {
		self.lastname = lastname
	}
	
	func getEmail() -> String {
		return email
	}
	
	mutating func setEmail(_ email: String) {
		self.email = email
	}
	
	func getPhoto() -> String {
		return photo
	}
	
	mutating func setPhoto(_ photo: String) {
		self.photo = photo
	}
	
	func getBirthDate() -> Date {
		return birthDate
	}
	
	mutating func setBirthDate(_ birthDate: Date) {
		self.birthDate = birthDate
	}
	
	func getGroups() -> [Group] {
		return groups
	}
	
	mutating func setGroups(_ groups: [Group]) {
		self.groups = groups
	}
	
	func getActivities() -> [Activity] {
		return activities
	}
	
	mutating func setActivities(_ activities: [Activity]) {
		self.activities = activities
	}
	
	func getEvents() -> [Event] {
		return events
	}
	
	mutating func setEvents(_ events: [Event]) {
		self.events = events
	}
	
	func getCommunities() -> [Community] {
		return communities
	}
	
	mutating func setCommunities(_ communities: [Community]) {
		self.communities = communities
	}
	
	func getHobbies() -> [Hobby] {
		return hobbies
	}
	
	mutating func setHobbies(_ hobbies: [Hobby]) {
		self.hobbies = hobbies
	}
}
