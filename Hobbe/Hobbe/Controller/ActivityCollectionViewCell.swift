//
//  ActivityCollectionViewCell.swift
//  Hobbe
//
//  Created by Norberto Duarte M on 17/10/20.
//

import UIKit

class ActivityCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var activityNameLabel: UILabel!
    @IBOutlet weak var activityTemperatureLabel: UILabel!
    
    @IBOutlet weak var activityImageView: UIImageView!

    
    func configure(activityName: String, activityTemperature : Double){
        activityNameLabel.text = activityName
        activityTemperatureLabel.text = "Temp: \(activityTemperature)"
        activityImageView.image = #imageLiteral(resourceName: "cycling")
    }
    
}
