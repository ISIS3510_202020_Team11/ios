//
//  HobbyTableViewController.swift
//  Hobbe
//
//  Created by Andres Donoso on 3/11/20.
//

import UIKit

private typealias ListHobby = (hobby: Hobby, isSelected: Bool)

class HobbyTableViewController: UITableViewController {
	@IBOutlet weak var searchBar: UISearchBar!
	
	//MARK: - Local variables
	fileprivate var hobbies = [ListHobby]()
	fileprivate var filteredHobbies = [ListHobby]()
	var selectedHobbies = Hobbies()
	weak var delegate: HobbyTableDelegate?
	let hobbyService = HobbyService()
	
	//MARK: - View Life Cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		hobbyService.getHobbies { (hobbies, error) in
			if let error = error {
				print(error)
			} else {
				for hobby in hobbies! {
					self.hobbies.append((hobby: hobby, isSelected: false))
				}
				
				self.hobbies.sort {
					$0.hobby.getName() < $1.hobby.getName()
				}
				
				self.filteredHobbies = self.hobbies
				
				DispatchQueue.main.async {
					self.tableView.reloadData()
				}
			}
		}
		
		searchBar.delegate = self
	}
}

//MARK: - UITableView Data Source

extension HobbyTableViewController {
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return filteredHobbies.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Storyboard.hobbyCellIdentifier, for: indexPath) as? HobbyTableViewCell else {
			fatalError("The dequeued cell is not an instance of HobbyTableViewCell")
		}
		
		let listHobby = filteredHobbies[indexPath.row]
		
		cell.hobbyNameLabel.text = listHobby.hobby.getName()
		cell.accessoryType = listHobby.isSelected ? .checkmark : .none
		
		return cell
	}
}

//MARK: - UITableView Delegate

extension HobbyTableViewController {
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		var listHobby = filteredHobbies[indexPath.row]
		
		listHobby.isSelected = !listHobby.isSelected
		
		if listHobby.isSelected {
			selectedHobbies.append(listHobby.hobby)
		} else {
			selectedHobbies = selectedHobbies.filter {
				$0.getId() != listHobby.hobby.getId()
			}
		}
		
		let index = hobbies.firstIndex {
			$0.hobby.getId() == listHobby.hobby.getId()
		}
		
		hobbies[index!] = listHobby
		filteredHobbies[indexPath.row] = listHobby
		
		delegate?.reloadSelectedHobbies(selectedHobbies)
		
		tableView.reloadData()
		
		tableView.deselectRow(at: indexPath, animated: true)
	}
}

//MARK: - UISearchBar Delegate

extension HobbyTableViewController: UISearchBarDelegate {
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		filteredHobbies = hobbies
		
		if !searchText.isEmpty {
			filteredHobbies = hobbies.filter({
				$0.hobby.getName().contains(searchText)
			})
		}
		
		tableView.reloadData()
	}
}
