//
//  ExploreViewController.swift
//  Hobbe
//
//  Created by Andres Donoso on 15/11/20.
//

import UIKit
import Koloda

private var numberOfCards: Int = 5

class ExploreViewController: UIViewController {
	private let eventService = EventService()
    private let userService = UserService()
	
	//MARK: - Outlets
	@IBOutlet weak var kolodaView: KolodaView!
	@IBOutlet weak var skipButton: UIButton!
	@IBOutlet weak var likeButton: UIButton!
	
	fileprivate var dataSource: Events = [] /*= {
		var events: Events = []
		
		events.append(Event(id: 0, name: "Activity 1", location: "Joshue Tree", date: Date(), hour: "23:00", image: "cycling"))
		events.append(Event(id: 1, name: "Activity 2", location: "Buenavista", date: Date(), hour: "12:00", image: "kayaking"))
		events.append(Event(id: 2, name: "Activity 3", location: "Parque Explora", date: Date(), hour: "16:00", image: "music"))
		events.append(Event(id: 3, name: "Activity 4", location: "Salitre Mágico", date: Date(), hour: "8:00", image: "swimming"))
		
		return events
	}()*/
    
    fileprivate var hobbies = [String]()
    fileprivate var hobbiesNot = [String]()
	
    override func viewDidLoad() {
        super.viewDidLoad()

		kolodaView.delegate = self
		kolodaView.dataSource = self
        
        if(!NetworkManager.isConnected()){
            self.view.makeToast("No network connection", duration: 1800.0)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatus), name: Notification.Name("networkChange"), object: nil)
		
		DispatchQueue.global(qos: .background).async {
			self.eventService.getActivitiesExplore { [unowned self] (cache, error) in
				if let cache = cache {
					self.dataSource = cache.object(forKey: "exploreActivities")?.getEvents() ?? []
					
					for i in 0..<self.dataSource.count {
						self.dataSource[i].setImage("swimming")
					}
					
					DispatchQueue.main.async {
						self.kolodaView.reloadData()
					}
				}
			}
		}
		
		designButtons()
    }
    
    @objc func networkStatus(){
        if(!NetworkManager.isConnected()){
            self.view.makeToast("No network connection", duration: 1800.0)
        }else{
            self.view.hideAllToasts() }
    }

    //MARK: - IBActions
	
	@IBAction func skipButtonPressed(_ sender: UIButton) {
		kolodaView.swipe(.left)
	}
	
	@IBAction func likeButtonPressed(_ sender: UIButton) {
		kolodaView.swipe(.right)
	}
	
	//MARK: - Methods
	
	private func designButtons() {
		let cornerRadius = skipButton.frame.size.height / 2
		
		// Skip button
		skipButton.layer.cornerRadius = cornerRadius
		skipButton.imageView?.layer.cornerRadius = cornerRadius
		skipButton.imageView?.contentMode = .scaleAspectFill
		
		// Like button
		likeButton.layer.cornerRadius = cornerRadius
		likeButton.imageView?.layer.cornerRadius = cornerRadius
		likeButton.imageView?.contentMode = .scaleAspectFill
	}
}

//MARK: - KolodaViewDelegate

extension ExploreViewController: KolodaViewDelegate {
	func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
		koloda.reloadData()
		skipButton.isEnabled = false
		likeButton.isEnabled = false
        
        userService.sendActivities(hobbies: hobbies, hobbiesNot: hobbiesNot) { [unowned self] (response, error) in
            if error != nil {
                self.showAlert(title: "Error", message: error!)
            } else {
                print(response!)
            }
        }
	}
	
	func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
		print("Selected card \(index)")
	}
	
	func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
		print("Selected \(index): \(dataSource[index].getName())")
        
        switch direction {
            case .left:
                hobbiesNot.append(dataSource[index].getCategory())
                
            case .topLeft:
                hobbiesNot.append(dataSource[index].getCategory())
                
            case .bottomLeft:
                hobbiesNot.append(dataSource[index].getCategory())
                
            default:
                hobbies.append(dataSource[index].getCategory())
        }
	}
}

//MARK: - KolodaDataSource

extension ExploreViewController: KolodaViewDataSource {
	func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
		let view = ActivityCardView()
		let event = dataSource[index]
		var date = ""
		
		view.imageView.image = UIImage(named: event.getImage())
		view.nameLabel.text = event.getName()
		view.placeLabel.text = event.getLocation()
		
		if Calendar.current.isDateInToday(event.getDate()) {
			date = "Today at "
		} else if Calendar.current.isDateInTomorrow(event.getDate()) {
			date = "Tomorrow at "
		} else {
			let df = DateFormatter()
			df.dateFormat = "MMMM d"
			date = "\(df.string(from: event.getDate())) at "
		}
		
		view.dateLabel.text = "\(date)\(event.getHour())"
		
		return view
	}
	
	func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
		return dataSource.count
	}
	
	func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
		return .default
	}
	
	func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
		return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)?[0] as? OverlayView
	}
}
