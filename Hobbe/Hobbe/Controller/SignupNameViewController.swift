//
//  SignUpViewController.swift
//  Hobbe
//
//  Created by Andres Donoso on 16/10/20.
//

import UIKit

class SignupNameViewController: UIViewController {
	@IBOutlet weak var progressBar: HorizontalProgressBar!
	@IBOutlet weak var firstNameTextField: UITextField!
	@IBOutlet weak var lastNameTextField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		progressBar.progress = 0.10
		self.hideKeyboardWhenTappedAround()
	}
	
	
	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let vc = segue.destination as? SignupBirthdateViewController {
			var user = User()
			user.setName(firstNameTextField.text!)
			user.setLastname(lastNameTextField.text!)
			
			vc.user = user
		}
	}
	
	/**
	Checks if the first name and the last name are not empty.
	If either one of them is empty, it returns false.
	*/
	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
		if let firstName = firstNameTextField.text, let lastName = lastNameTextField.text {
			if firstName.isEmpty || lastName.isEmpty {
				let alert = UIAlertController(title: "Empty Fields", message: "Your first name and last name should not be empty.", preferredStyle: .alert)
				
				alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
				
				self.present(alert, animated: true, completion: nil)
				
				return false
			} else {
				progressBar.progress = 0.33
				
				return true
			}
		}
		
		return false
	}
}
