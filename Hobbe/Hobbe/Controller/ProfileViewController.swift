//
//  ProfileViewController.swift
//  Hobbe
//
//  Created by Andres Donoso on 6/11/20.
//

import UIKit

class ProfileViewController: UIViewController {
	@IBOutlet weak var userImage: UIImageView!
	
    override func viewDidLoad() {
        super.viewDidLoad()

		userImage.layer.masksToBounds = true
		userImage.layer.cornerRadius = userImage.bounds.width / 2
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
