//
//  EventDetailViewController.swift
//  Hobbe
//
//  Created by Valentina Duarte Benítez on 10/11/2020.
//

import UIKit
import MapKit
import FirebaseCrashlytics
import Toast_Swift

class EventDetailViewController: UIViewController {
    
    var selectedActivity : Event?
    
    var isConnected : Bool?
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    @IBOutlet weak var eventNameLabel: UILabel!
    
    @IBOutlet weak var eventDateLabel: UILabel!
    
    
    @IBOutlet weak var eventHourLabel: UILabel!
    
    @IBOutlet weak var eventCostLabel: UILabel!
    
    @IBOutlet weak var eventOrganizerLabel: UILabel!
    @IBOutlet weak var eventAddressLabel: UILabel!
    
    @IBOutlet weak var eventDescriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height+100)
        
        if(!NetworkManager.isConnected()){
            self.view.makeToast("No network connection", duration: 1800.0)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(networkStatus), name: Notification.Name("networkChange"), object: nil)
        
        let url = URL(string: "https://www.fillmurray.com/640/360")
        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        
        if(data==nil){
            self.imageView.image = #imageLiteral(resourceName: "cycling")
        }else{
            imageView.image = UIImage(data: data!)
        }
        
        //self.imageView.image = #imageLiteral(resourceName: "cycling")
        
        self.eventNameLabel.text = self.selectedActivity?.getName();
        
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let actDate = self.selectedActivity?.getDate()
        let date = df.string(from: actDate!)
        
        
        self.eventDateLabel.text = date
        
        self.eventHourLabel.text = self.selectedActivity?.getHour()
        self.eventCostLabel.text = self.selectedActivity?.getCost()
        self.eventOrganizerLabel.text = self.selectedActivity?.getOrganizer()
        self.eventAddressLabel.text = self.selectedActivity?.getAddress()
        self.eventDescriptionLabel.text = self.selectedActivity?.getDescription()
        
        let lat = self.selectedActivity?.getLat()
        
        let lon = self.selectedActivity?.getLon()
        
        let location = CLLocation(latitude: lat!, longitude: lon!)
        
        mapView.centerToLocation(location)
        
        
        fetchLocationOnMap(self.selectedActivity!)
        
       /*let button = UIButton(type: .roundedRect)
                button.frame = CGRect(x: 20, y: 50, width: 100, height: 30)
                button.setTitle("Crash", for: [])
                button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
                view.addSubview(button)*/
    }
    
   /* @IBAction func crashButtonTapped(_ sender: AnyObject) {
        fatalError()
      }*/
    
    
    @objc func networkStatus(){
        
        //let isConnected = NetworkManager.isConnected()
        
        if(!NetworkManager.isConnected()){
            self.view.makeToast("No network connection", duration: 1800.0)
        }else{
            self.view.hideAllToasts() }
    }
    
    func fetchLocationOnMap(_ event : Event){
        let annotation = MKPointAnnotation()
        annotation.title = event.getName()
        
        annotation.coordinate = CLLocationCoordinate2D(latitude: event.getLat(), longitude: event.getLon())
        mapView.addAnnotation(annotation)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

private extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance = 25000
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}
