//
//  SettingsTableViewController.swift
//  Hobbe
//
//  Created by Andres Donoso on 6/11/20.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

	
	@IBAction func logOutPressed(_ sender: UIButton) {
		if Authentication.logOut() {
			let welcomeNVC = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.welcomeNVC) as! NavigationViewController
			
			self.view.window?.rootViewController = welcomeNVC
			self.view.window?.makeKeyAndVisible()
		} else {
			self.showAlert(title: "Error", message: "An error occured when trying to log you out. Try again later.")
		}
	}
}
