//
//  HobbyTableViewCell.swift
//  Hobbe
//
//  Created by Andres Donoso on 3/11/20.
//

import UIKit

class HobbyTableViewCell: UITableViewCell {
	@IBOutlet weak var hobbyNameLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
