//
//  ViewController.swift
//  Hobbe
//
//  Created by Andres Donoso on 8/10/20.
//

import UIKit
import CoreLocation
import FirebaseAuth
import Toast_Swift

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, WeatherManagerDelegate {
	
	//MARK: - Outlets
	@IBOutlet weak var welcomeLabel: UILabel!
	@IBOutlet weak var activityCollectionView: UICollectionView!
	@IBOutlet weak var trendingCollectionView: UICollectionView!
	
	//MARK: - Local variables
	var email: String?
	var userTemperature = 28.0
	var selectedActivity: Event?
	
	var activities: [Event]?
	
	var trendingActivities: [Event]?
	
	let locationManager = CLLocationManager()
	
	let dispatchGroup = DispatchGroup()
	
	var isApiCalling = false
	
	let recommendationService = RecommendationService()
	
	let trendingEventsService = EventService()
	
	//MARK: - Computed properties
	private var greeting: String {
		let currentHour = Calendar.current.component(.hour, from: Date())
		
		switch currentHour {
			case 0...11:
				return "Good morning, "
				
			case 12...17:
				return "Good afternoon, "
				
			default:
				return "Good night, "
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if(!NetworkManager.isConnected()){
			self.view.makeToast("No network connection", duration: 1800.0)
		}
		
		NotificationCenter.default.addObserver(self, selector: #selector(networkStatus), name: Notification.Name("networkChange"), object: nil)
		
		locationManager.delegate = self
		locationManager.requestWhenInUseAuthorization()
		self.locationManager.requestLocation()
	}
	
	@objc func networkStatus(){
		if(!NetworkManager.isConnected()){
			self.view.makeToast("No network connection", duration: 1800.0)
		}else{
			self.view.hideAllToasts() }
	}
	
	//MARK: - IBActions
	@IBAction func recommendationPressed(_ sender: UIButton) {
		self.locationManager.requestLocation()
	}
	
	//MARK: - CollectionView
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		if(collectionView == activityCollectionView){
			let lay = collectionViewLayout as! UICollectionViewFlowLayout
			let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
			return CGSize(width:widthPerItem, height:120)
		}else{
			return CGSize(width:150, height:100)
		}
		
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		if(collectionView == activityCollectionView){
			return activities?.count ?? 0
		} else{
			
			//TODO cambiar lo que esta por return trendingActivities?.count ?? 0
			//return activities?.count ?? 0
			return trendingActivities?.count ?? 0
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		if(collectionView == activityCollectionView){
			let cell = activityCollectionView.dequeueReusableCell(withReuseIdentifier: "activityCell", for: indexPath) as! ActivityCollectionViewCell
			
			cell.configure(activityName: activities?[indexPath.row].getName() ?? "", activityTemperature: activities?[indexPath.row].getTemperature() ?? 0)
			return cell
		}else{
			let cell = trendingCollectionView.dequeueReusableCell(withReuseIdentifier: "trendingCell", for: indexPath) as! TrendingCollectionViewCell
			
			//TODO Falta cambiar de activities a trendingActivities
			cell.configure(trendingName: trendingActivities?[indexPath.row].getName() ?? "", trendingTemperature: trendingActivities?[indexPath.row].getTemperature() ?? 0)
			
			return cell
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if(segue.identifier == "goToEventDetail"){
			let destinationViewController = segue.destination as! EventDetailViewController
			destinationViewController.selectedActivity = self.selectedActivity
		}
		
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		if(collectionView == activityCollectionView){
			selectedActivity = activities?[indexPath.row]
            let id = selectedActivity?.id
            UserService().setInteractions(idActivity: id!) { (response, error) in
                if let error = error {
                    self.showAlert(title: "Error", message: error)
                }
            }
			self.performSegue(withIdentifier: "goToEventDetail", sender: self)
		} else{
			selectedActivity = trendingActivities?[indexPath.row]
            let id = selectedActivity?.id
            UserService().setInteractions(idActivity: id!) { (response, error) in
                if let error = error {
                    self.showAlert(title: "Error", message: error)
                }
            }
			self.performSegue(withIdentifier: "goToEventDetail", sender: self)
		}
		
		//TODO Descomentar lo anterior y borrar lo siguiente
		//selectedActivity = activities?[indexPath.row]
		//self.performSegue(withIdentifier: "goToEventDetail", sender: self)
	}
	
	//MARK: - Methods
	func didUpdateWeather( temp: Double){
		DispatchQueue.main.async {
			self.userTemperature = temp
			self.isApiCalling = false
		}
	}
	
	func didFailWithError(error: Error) {
		print(error)
	}
	
	func getRecommendedActivities() {
		print("trae las actividades")
		print(userTemperature)
		let eventsCached = recommendationService.getRecommendations { [unowned self] (events, error) in
			if error != nil {
				print(error!)
				self.showAlert(title: "Error", message: error!)
			} else {
				DispatchQueue.main.async {
					let filteredEvents = self.filterEvents(events!)
					
					self.activities = filteredEvents
					self.activityCollectionView.reloadData()
					//TODO quitar este reload
					//self.trendingCollectionView.reloadData()
				}
			}
		}
		
		if let events = eventsCached {
			let filteredEvents = filterEvents(events)
			DispatchQueue.main.async {
				self.activities = filteredEvents
				self.activityCollectionView.reloadData()
				//TODO quitar este reload
				//self.trendingCollectionView.reloadData()
			}
		}
	}
	
	func getTrendingActivities() {
		let eventsCached = trendingEventsService.getTrending{ [unowned self] (events, error) in
			if error != nil {
				print(error!)
				self.showAlert(title: "Error", message: error!)
			} else {
				DispatchQueue.main.async {
					let filteredTrendingEvents = self.filterEvents(events!)
					
					self.trendingActivities = filteredTrendingEvents
					self.trendingCollectionView.reloadData()
				}
			}
		}
		if let events = eventsCached {
			let filteredTrendingEvents = filterEvents(events)
			DispatchQueue.main.async {
				self.activities = filteredTrendingEvents
				self.trendingCollectionView.reloadData()
			}
		}
	}
	
	private func filterEvents(_ events: [Event]) -> [Event] {
		let filteredEvents = events.filter({ (event) -> Bool in
			let range = self.userTemperature-15...self.userTemperature+15
			return range.contains(event.temperature)
		})
		
		return filteredEvents
	}
}

extension HomeViewController: CLLocationManagerDelegate{
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if let location = locations.last{
			locationManager.stopUpdatingLocation()
			let latitude = location.coordinate.latitude
			let longitude = location.coordinate.longitude
			
			if !self.isApiCalling {
				self.isApiCalling = true
				
				WeatherService().getWeather(latitude, longitude) { (response, error) in
					if let error = error {
						self.showAlert(title: "Error", message: error)
					}else{
						self.userTemperature = response!
						print("Response \(response!)")
						self.didUpdateWeather(temp:  self.userTemperature)
						self.getRecommendedActivities()
						self.getTrendingActivities()
					}
				}
			}
		}
	}
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		print(error)
	}
}
