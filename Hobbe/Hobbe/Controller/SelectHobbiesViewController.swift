//
//  SelectHobbiesViewController.swift
//  Hobbe
//
//  Created by Andres Donoso on 3/11/20.
//

import UIKit

protocol HobbyTableDelegate: class {
	func reloadSelectedHobbies(_ hobbies: Hobbies)
}

class SelectHobbiesViewController: UIViewController {
	//MARK: - Outlets
	@IBOutlet weak var selectedHobbiesLabel: UILabel!
	@IBOutlet weak var userNameLabel: UILabel!
	
	//MARK: - Local variables
	var user: User?
	let userService = UserService()

    override func viewDidLoad() {
        super.viewDidLoad()
		
		if let name = user?.getName() {
			userNameLabel.text = ", \(name)"
		}
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		switch segue.destination {
			case let hobbyTableVC as HobbyTableViewController:
				hobbyTableVC.delegate = self
				
			default:
				break
		}
    }

	@IBAction func continueBtnPressed(_ sender: UIButton) {
		userService.saveUserInfo(name: user!.getName(), lastName: user!.getLastname(), birthdate: user!.getBirthDate(), hobbies: user!.getHobbies()) { [unowned self] (_, error) in
			if let error = error {
				self.showAlert(title: "Error", message: error)
			} else {
				DispatchQueue.main.async {
					let homeVC = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.tabBarController) as! UITabBarController
					
					self.view.window?.rootViewController = homeVC
					self.view.window?.makeKeyAndVisible()
				}
			}
		}
	}
}

//MARK: - HobbyTableDelegate

extension SelectHobbiesViewController: HobbyTableDelegate {
	func reloadSelectedHobbies(_ hobbies: Hobbies) {
		var message = "Selected hobbies: "
		
		for i in 0..<hobbies.count {
			if i == hobbies.count - 1 {
				message += "\(hobbies[i].getName())"
			} else {
				message += "\(hobbies[i].getName()), "
			}
		}
		
		selectedHobbiesLabel.text = message
		user?.setHobbies(hobbies)
	}
}
