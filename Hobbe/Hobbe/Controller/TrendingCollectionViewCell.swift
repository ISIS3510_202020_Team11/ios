//
//  TrendingCollectionViewCell.swift
//  Hobbe
//
//  Created by Valentina Duarte Benítez on 13/11/2020.
//

import UIKit

class TrendingCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var trendingImageView: UIImageView!
    
    @IBOutlet weak var trendingNameLabel: UILabel!
    @IBOutlet weak var trendingTemperatureLabel: UILabel!
    
    func configure(trendingName: String, trendingTemperature : Double){
        trendingNameLabel.text = trendingName
        trendingTemperatureLabel.text = "Temp: \(trendingTemperature)"
        trendingImageView.image = #imageLiteral(resourceName: "explore")
    }
    
}
