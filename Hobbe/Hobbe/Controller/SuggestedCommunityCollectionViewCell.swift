//
//  SuggestedCommunityCollectionViewCell.swift
//  Hobbe
//
//  Created by Norberto Duarte M on 15/10/20.
//

import UIKit

class SuggestedCommunityCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var communityNameLabel: UILabel!
    @IBOutlet weak var communityImageView: UIImageView!
    
    @IBOutlet weak var communityTypeLabel: UILabel!
    
    
    @IBOutlet weak var communityFlagImageView: UIImageView!
    
    
    func configure(communityName: String, communityImage : UIImage, communityType: String, communityFlag: UIImage){
        communityNameLabel.text = communityName;
        communityImageView.image = communityImage;
        communityTypeLabel.text = communityType;
        communityFlagImageView.image = communityFlag
        communityFlagImageView.layer.borderWidth=1.5
        communityFlagImageView.layer.borderColor = UIColor.white.cgColor
    }
}
