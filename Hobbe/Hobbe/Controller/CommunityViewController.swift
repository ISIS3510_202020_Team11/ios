//
//  CommunityViewController.swift
//  Hobbe
//
//  Created by Norberto Duarte M on 15/10/20.
//

import UIKit

class CommunityViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    let ownCommunities = [
        Community(name: "Swimming", description: "A swimming community for sport lovers", type: "World", posts: [], image: "swimming"),
        Community(name: "Music", description: "Everything a music lover needs", type: "Colombia", posts: [], image: "music"),
        Community(name: "Swimming", description: "A swimming community for sport lovers", type: "World", posts: [], image: "swimming")
        ]
    
    let suggestedCommunities = [
        Community(name: "Swimming", description: "A swimming community for sport lovers", type: "World", posts: [], image: "swimming"),
        Community(name: "Music", description: "Everything a music lover needs", type: "World", posts: [], image: "music"),
        Community(name: "Kayaking", description: "A kayaking community for colombian sport lovers", type: "Colombia", posts: [], image: "kayaking")
    ]

    @IBOutlet weak var ownCommunitiesCollectionView: UICollectionView!
    @IBOutlet weak var suggestedCommunitiesCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(collectionView == ownCommunitiesCollectionView){
            let lay = collectionViewLayout as! UICollectionViewFlowLayout
            let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing

            return CGSize(width:widthPerItem, height:100)
        } else{
            let lay = collectionViewLayout as! UICollectionViewFlowLayout
            let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing

            return CGSize(width:widthPerItem, height:100)
        }      
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == ownCommunitiesCollectionView){
            return ownCommunities.count
        } else{
            return suggestedCommunities.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == ownCommunitiesCollectionView){
            let cell = ownCommunitiesCollectionView.dequeueReusableCell(withReuseIdentifier: "ownCell", for: indexPath) as! OwnCommunityCollectionViewCell
            let img = UIImage(named: ownCommunities[indexPath.row].getImage()) ?? #imageLiteral(resourceName: "kayaking")
            let flag = UIImage(named: ownCommunities[indexPath.row].getType()) ?? #imageLiteral(resourceName: "World")
            
            cell.configure(communityName: ownCommunities[indexPath.row].getName(), communityImage: img, communityType: ownCommunities[indexPath.row].getType(), communityFlag: flag)
            return cell
            
        }else{
            let cell = suggestedCommunitiesCollectionView.dequeueReusableCell(withReuseIdentifier: "suggestedCell", for: indexPath) as! SuggestedCommunityCollectionViewCell
            let img = UIImage(named: suggestedCommunities[indexPath.row].getImage()) ?? #imageLiteral(resourceName: "kayaking")
            let flag = UIImage(named: suggestedCommunities[indexPath.row].getType()) ?? #imageLiteral(resourceName: "World")
            cell.configure(communityName: suggestedCommunities[indexPath.row].getName(), communityImage: img, communityType: suggestedCommunities[indexPath.row].getType(), communityFlag: flag)
            return cell
        }
        
    }
       
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selecciono ")
    }

}
