//
//  SignupUserViewController.swift
//  Hobbe
//
//  Created by Andres Donoso on 16/10/20.
//

import UIKit

class SignupUserViewController: UIViewController {
	//MARK: - Outlets
	@IBOutlet weak var progressBar: HorizontalProgressBar!
	@IBOutlet weak var emailTextField: UITextField! {
		didSet {
			emailTextField.tintColor = .black
			emailTextField.setIcon(UIImage(systemName: "envelope")!, isSquare: false)
		}
	}
	
	@IBOutlet weak var passwordTextField: UITextField! {
		didSet {
			passwordTextField.tintColor = .black
			passwordTextField.setIcon(UIImage(systemName: "lock")!)
		}
	}
	
	@IBOutlet weak var passwordVTextField: UITextField! {
		didSet {
			passwordVTextField.tintColor = .black
			passwordVTextField.setIcon(UIImage(systemName: "lock")!)
		}
	}
	
	//MARK: - Local variables
	var user: User?
	private let userManager = UserService()
	
	//MARK: - ViewDidLoad
	override func viewDidLoad() {
		super.viewDidLoad()
		
		progressBar.progress = 0.66
		self.hideKeyboardWhenTappedAround()
	}
	
	//MARK: - IBActions
	@IBAction func signUpPressed(_ sender: UIButton) {
		if let email = emailTextField.text, let password = passwordTextField.text, let passwordV = passwordVTextField.text {
			var title = ""
			var message = ""
			
			if !email.isEmpty && !password.isEmpty && !passwordV.isEmpty {
				
				//Checking if the email is formatted correctly
				if email ~= "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$" {
					
					//Checking if the password has at least 6 characters
					if password.count >= 6 {
						
						//Checking if the password is the same as the verifyPassword
						if password == passwordV {
							Authentication.signUp(withEmail: email, password) { [unowned self] (title, message) in
								if let errorTitle = title, let errorMessage = message {
									self.showAlert(title: errorTitle, message: errorMessage)
								} else {
									
									let selectHobbiesVC = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.selectHobbiesViewController) as! SelectHobbiesViewController
									
									selectHobbiesVC.user = self.user
									
									self.view.window?.rootViewController = selectHobbiesVC
									self.view.window?.makeKeyAndVisible()
								}
							}
						} else {
							title = "Password Mismatch"
							message = "The passwords entered don't match."
						}
					} else {
						title = "Weak Password"
						message = "The password must be at least 6 characters long."
					}
				} else {
					title = "Invalid Email"
					message = "The format of the email is invalid."
				}
			} else {
				title = "Missing Fields"
				message = "Please fill all of the fields."
			}
			
			if !title.isEmpty && !message.isEmpty {
				self.showAlert(title: title, message: message)
			}
		}
	}
}
