//
//  LoginViewController.swift
//  Hobbe
//
//  Created by Norberto Duarte M on 15/10/20.
//

import UIKit

class LoginViewController: UIViewController {
	
	
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.hideKeyboardWhenTappedAround()
	}
	
	@IBAction func signInPressed(_ sender: UIButton) {
		emailTextField.endEditing(true)
		passwordTextField.endEditing(true)
		if let email = emailTextField.text, let password = passwordTextField.text {
			if email.isEmpty || password.isEmpty {
				let alert = UIAlertController(title: "Empty Fields", message: "Your email and password should not be empty.", preferredStyle: .alert)
				
				alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
				
				self.present(alert, animated: true, completion: nil)
				
			} else {
				Authentication.signIn(with: email, password) { (title, message) in
					if let errorTitle = title, let errorMessage = message {
						let alert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
						
						alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
						self.present(alert, animated: true, completion: nil)
					}else{
						let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.tabBarController) as? UITabBarController
						
						self.view.window?.rootViewController = homeViewController
						self.view.window?.makeKeyAndVisible()
					}
				}
				
			}
		}
		
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
