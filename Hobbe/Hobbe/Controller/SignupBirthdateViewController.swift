//
//  SignupBirthdateViewController.swift
//  Hobbe
//
//  Created by Andres Donoso on 16/10/20.
//

import UIKit

class SignupBirthdateViewController: UIViewController {
	@IBOutlet weak var progressBar: HorizontalProgressBar!
	@IBOutlet weak var datePicker: UIDatePicker!
	
	var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()
		
		progressBar.progress = 0.33
		datePicker.date = Calendar.current.date(byAdding: .year, value: -18, to: Date()) ?? Date()
    }
    

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let vc = segue.destination as? SignupUserViewController {
			user?.setBirthDate(datePicker.date)
			
			vc.user = user
		}
    }
	
	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
		
		//Checks if the user is an adult
		//FIXME: - Is everybody an adult at 18? What about other countries?
		let dateComponents = Calendar.current.dateComponents([.year], from: datePicker.date, to: Date())
		let age = dateComponents.year!
		
		if age < 18 {
			let alert = UIAlertController(title: "Insufficient Age", message: "You must be at least 18 to use the application.", preferredStyle: .alert)
			
			alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
			
			self.present(alert, animated: true, completion: nil)
		} else {
			progressBar.progress = 0.66
		}
		
		return age >= 18
	}

}
