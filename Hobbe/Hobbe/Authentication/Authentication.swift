//
//  Authentication.swift
//  Hobbe
//
//  Created by Andres Donoso on 15/10/20.
//

import Foundation
import FirebaseAuth

/**
Contains all of the authentication methods with Firebase.
*/
struct Authentication {
	
	/**
	Signs up a user with his email and password.
	
	- Parameters:
		- email: The email of the user.
		- password: The password of the user.
		- email: The email of the user.
		- completionHandler: A function for handling the errors on the View.
		- errorTitle: The title of the error.
		- errorMessage: A description of the error.
	*/
	public static func signUp(withEmail email: String, _ password: String, completionHandler: @escaping (_ errorTitle: String?, _ errorMessage: String?) -> Void) {
		Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
			var errorTitle = ""
			var errorMessage = ""
			
			if let error = error as NSError? {
				guard let errorCode = AuthErrorCode(rawValue: error.code) else {
					print("There was an error but it couldn't be matched with a Firebase code.")
					return
				}
				
				switch errorCode {
					case .emailAlreadyInUse:
						errorTitle = "Email Already Exists"
						errorMessage = "There's already an account with this email. Try logging in."
						
					case .invalidEmail:
						errorTitle = "Invalid Email"
						errorMessage = "The email you entered seems to have a format error."
						
					case .networkError:
						errorTitle = "Network Error"
						errorMessage = "There was a network error when trying to sign up. Please try again later."
						
					default:
						errorTitle = "Error"
						errorMessage = "An error occured. Please try again later."
						print(error.localizedDescription)
				}
				
				completionHandler(errorTitle, errorMessage)
			} else {
				completionHandler(nil, nil)
			}
		}
	}
    
    /**
    Signs in a user with his email and password.
    
    - Parameters:
        - email: The email of the user.
        - password: The password of the user.
        - completionHandler: A function for handling the errors on the View.
        - errorTitle: The title of the error.
        - errorMessage: A description of the error.
    */
    
    public static func signIn(with email: String, _ password: String, completionHandler: @escaping (_ errorTitle: String?, _ errorMessage: String?) -> Void) -> Void{
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            var errorTitle = ""
            var errorMessage = ""
            
            if let error = error as NSError? {
                guard let errorCode = AuthErrorCode(rawValue: error.code) else {
                    print("There was an error but it couldn't be matched with a Firebase code.")
                    return
                }
                switch errorCode {
                    case .userNotFound:
                        errorTitle = "Account not found"
                        errorMessage = "You don't have an account. Try signing up"
                        
                    case .wrongPassword:
                        errorTitle = "Wrong Password"
                        errorMessage = "The password you entered isn't correct. Please try again, or click on forgot password."
                        
                    case .invalidEmail:
                        errorTitle = "Invalid Email"
                        errorMessage = "The email you entered seems to have a format error."
                        
                    case .networkError:
                        errorTitle = "Network Error"
                        errorMessage = "There was a network error when trying to sign up. Please try again later."
                        
                    default:
                        errorTitle = "Error"
                        errorMessage = "An error occured. Please try again later."
                        print(error.localizedDescription)
                        
                }
                
                completionHandler(errorTitle, errorMessage)
               
            }else{
                completionHandler(nil,nil)
            }
            
        }
    }
    
	/**
	Refreshes the authentication token of the current user.
	*/
	public static func refreshToken() {
		Auth.auth().currentUser?.getIDTokenForcingRefresh(true, completion: nil)
	}
	
	/**
	Signs out the current user.
	- Returns: `true` if logout was successful, `false` if not.
	*/
	public static func logOut() -> Bool {
		do {
			try Auth.auth().signOut()
			return true
		} catch {
			return false
		}
	}
}
