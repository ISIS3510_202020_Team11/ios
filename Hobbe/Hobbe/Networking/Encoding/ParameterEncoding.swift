//
//  ParameterEncoding.swift
//  Hobbe
//
//  Created by Andres Donoso on 10/10/20.
//

import Foundation

public typealias Parameters = [String:Any]

public protocol ParameterEncoder {
	/**
	Encodes the parameters.
	- Parameters:
		- urlRequest: Url used for the request.
		- parameters: Request parameters.
	
	- Throws:
		- `NetworkError.parametersNil` => if `parameters == nil`.
		- `NetworkError.encodingFailed` => if parameter encoding failed.
		- `NetworkError.missingURL` => if `urlRequest == nil`.
	*/
	static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}

public enum NetworkError: String, Error {
	case parametersNil = "Parameters were nil."
	case encodingFailed = "Parameter encoding failed."
	case missingURL = "URL is nil."
}
