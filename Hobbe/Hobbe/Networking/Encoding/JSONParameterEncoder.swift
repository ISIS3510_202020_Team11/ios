//
//  JSONParameterEncoder.swift
//  Hobbe
//
//  Created by Andres Donoso on 10/10/20.
//

import Foundation

/**
Encodes parameters to JSON and adds the appropriate headers.
*/
public struct JSONParameterEncoder: ParameterEncoder {
	public static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
		do {
			let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
			urlRequest.httpBody = jsonAsData
			if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
				urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
			}
		} catch {
			throw NetworkError.encodingFailed
		}
	}
}
