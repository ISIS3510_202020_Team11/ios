//
//  EndPointType.swift
//  Hobbe
//
//  Created by Andres Donoso on 10/10/20.
//

import Foundation

/**
Implementation that all endpoints should follow.
*/
protocol EndPointType {
	var baseURL: URL { get }
	var path: String { get }
	var httpMethod: HTTPMethod { get }
	var task: HTTPTask { get }
	var headers: HTTPHeaders? { get }
}
