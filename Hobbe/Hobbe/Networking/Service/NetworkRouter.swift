//
//  NetworkRouter.swift
//  Hobbe
//
//  Created by Andres Donoso on 10/10/20.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ()

/**
Implementation that a network router should follow.
*/
protocol NetworkRouter: class {
	/**
	Endpoint that the router should manage.
	*/
	associatedtype EndPoint: EndPointType
	
	/**
	Request made by the router.
	- Parameters:
		- route: Request's endpoint.
		- completion: Action to be done when the request is done.
	*/
	func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
	
	/**
	Cancel a request.
	*/
	func cancel()
}
