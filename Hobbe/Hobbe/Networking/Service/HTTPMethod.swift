//
//  HTTPMethod.swift
//  Hobbe
//
//  Created by Andres Donoso on 10/10/20.
//

import Foundation

/**
HTTP protocols
*/
public enum HTTPMethod: String {
	case get = "GET"
	case post = "POST"
	case put = "PUT"
	case delete = "DELETE"
}
