//
//  HTTPTask.swift
//  Hobbe
//
//  Created by Andres Donoso on 10/10/20.
//

import Foundation

/**
Type alias for a dictionary
*/
public typealias HTTPHeaders = [String:String]

/**
Responsible for configuring parameters for a specific endpoint.
*/
public enum HTTPTask {
	case request
	
	case requestParameters(bodyParameters: Parameters?, urlParameters: Parameters?)
	
	case requestParametersAndHeaders(bodyParameters: Parameters?, urlParameters: Parameters?, additionalHeaders: HTTPHeaders?)
}
