//
//  Router.swift
//  Hobbe
//
//  Created by Andres Donoso on 10/10/20.
//

import Foundation
import FirebaseAuth

/**
The router is in charge of making the request, i.e., build the request and send it.
*/
class Router<EndPoint: EndPointType>: NetworkRouter {
	private var task: URLSessionTask?
	
	func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
		let session = URLSession.shared
		do {
			let request = try self.buildRequest(from: route)
			task = session.dataTask(with: request, completionHandler: { (data, response, error) in
				completion(data, response, error)
			})
		} catch {
			completion(nil, nil, error)
		}
		self.task?.resume()
	}
	
	func cancel() {
		self.task?.cancel()
	}
	
	/**
	Builds the request from the given end point.
	- Parameters:
		- route: End point to which the request is made.
	
	- Throws: `NetworkError` if the encoders fail.
	
	- Returns: The constructed url.
	*/
	fileprivate func buildRequest(from route: EndPoint) throws -> URLRequest {
		var request = URLRequest(url: route.baseURL.appendingPathComponent(route.path), cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0)
		
		request.httpMethod = route.httpMethod.rawValue
		
		do {
			switch route.task {
				case .request:
					request.setValue("application/json", forHTTPHeaderField: "Content-Type")
					
				case .requestParameters(let bodyParameters, let urlParameters):
					try self.configureParameters(bodyParameters: bodyParameters, urlParameters: urlParameters, request: &request)
					
				case .requestParametersAndHeaders(let bodyParameters, let urlParameters, let additionalHeaders):
					self.addAdditionalHeaders(additionalHeaders, request: &request)
					try self.configureParameters(bodyParameters: bodyParameters, urlParameters: urlParameters, request: &request)
			}
			return request
		} catch {
			throw error
		}
	}
	
	/**
	Encodes the body and url parameters.
	- Parameters:
		- bodyParameters: Parameters sent in the body of the request.
		- urlParameters: Parameters sent in the url of the request.
		- request: The request.
	
	- Throws: `NetworkError` if the encoding fails.
	*/
	fileprivate func configureParameters(bodyParameters: Parameters?, urlParameters: Parameters?, request: inout URLRequest) throws {
		do {
			if let bodyParameters = bodyParameters {
				try JSONParameterEncoder.encode(urlRequest: &request, with: bodyParameters)
			}
			
			if let urlParameters = urlParameters {
				try URLParameterEncoder.encode(urlRequest: &request, with: urlParameters)
			}
		} catch {
			throw error
		}
	}
	
	/**
	Adds any additional header to the request.
	- Parameters:
		- additionalHeaders: Dictionary of additional headers.
		- request: The request.
	*/
	fileprivate func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
		guard let headers = additionalHeaders else { return }
		for (key, value) in headers {
			request.setValue(value, forHTTPHeaderField: key)
		}
	}
}
