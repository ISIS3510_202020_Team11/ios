//
//  BackendManager.swift
//  Hobbe
//
//  Created by Andres Donoso on 27/10/20.
//

import Foundation
import FirebaseAuth

struct BackendManager {
	private let router = Router<BackendApi>()
	
	/**
	Saves the user's info on Firebase.
	
	- Parameters:
		- name: First name of the user.
		- lastName: Last name of the user.
		- birthdate: Birthdate of the user.
		- completion: Completion handler for when the request is done.
		- response: Response of the request.
		- error: Error in the request.
	*/
	func saveUserInfo(_ name: String, _ lastName: String, _ birthdate: String, _ hobbies: [Int], completion: @escaping (_ response: String?, _ error: String?) -> ()) {
		let bodyParams = ["name": name, "lastName": lastName, "birthdate": birthdate, "hobbies": hobbies] as [String : Any]
		
		Auth.auth().currentUser?.getIDToken(completion: { (authToken, error) in
			if error != nil {
				print(error!)
				completion(nil, "There was an authentication error. Please try again.")
			} else {
				
				router.request(.saveUserInfo(bodyParams: bodyParams, authToken: authToken!)) { (data, response, error) in
					if error != nil {
						completion(nil, "Please check your network connection")
					} else {
					
						self.handleResponse(ofType: GenericResponse.self, data, response) { (decodedResponse, error) in
							completion(decodedResponse?.response, error)
						}
					}
				}
			}
		})
	}
	
	func getHobbies(completion: @escaping (_ response: Hobbies?, _ error: String?) -> ()) {
		Auth.auth().currentUser?.getIDToken(completion: { (token, error) in
			if error != nil {
				print(error!)
				completion(nil, "There was an authentication error. Please try again.")
			} else {
				
				router.request(.getHobbies(authToken: token!)) { (data, response, error) in
					if error != nil {
						completion(nil, "Please check your network connection")
					} else {
					
						self.handleResponse(ofType: Hobbies.self, data, response) { (decodedResponse, error) in
							completion(decodedResponse, error)
						}
					}
				}
			}
		})
	}
    
    func getEvents(completion: @escaping (_ response: Events?, _ error: String?) -> ()) {
        Auth.auth().currentUser?.getIDToken(completion: { (token, error) in
            if error != nil {
                print(error!)
                completion(nil, "There was an authentication error. Please try again.")
            } else {
                router.request(.getEvents(authToken: token!)) { (data, response, error) in
                    if error != nil {
                        completion(nil, "Please check your network connection")
                    } else {
                    
                        self.handleResponse(ofType: Events.self, data, response) { (decodedResponse, error) in
                            completion(decodedResponse, error)
                        }
                    }
                }
            }
        })
    }
	
	func getActivitiesExplore(completion: @escaping (_ response: Events?, _ error: String?) -> ()) {
		Auth.auth().currentUser?.getIDToken(completion: { (token, error) in
			if error != nil {
				completion(nil, "There was an authentication error. Please try again.")
			} else {
				router.request(.getActivitiesExplore(authToken: token!)) { (data, response, error) in
					if error != nil {
						completion(nil, "Please check your network connection")
					} else {
						self.handleResponse(ofType: Events.self, data, response) { (decodedResponse, error) in
							completion(decodedResponse, error)
						}
					}
				}
			}
		})
	}
    
    func sendActivities(hobbies: [String], hobbiesNot: [String], completion: @escaping (_ response: String?, _ error: String?) -> ()) {
        Auth.auth().currentUser?.getIDToken(completion: { (token, error) in
            if error != nil {
                completion(nil, "There was an authentication error. Please try again.")
            } else {
                router.request(.sendActivities(authToken: token!, hobbies: hobbies, hobbiesNot: hobbiesNot)) { (data, response, error) in
                    if error != nil {
                        completion(nil, "Please check your network connection")
                    } else {
                        self.handleResponse(ofType: GenericResponse.self, data, response) { (decodedResponse, error) in
                            completion(decodedResponse?.response, error)
                        }
                    }
                }
            }
        })
    }
	
	//MARK: - Private Methods
	
	/**
	Handles the response of the request.
	- Parameters:
		- data: The data received
		- response: The response received
		- completion: Completion handler
		- decodedResponse: API response of type T
		- error: Error.
	*/
	private func handleResponse<T: Decodable>(ofType type: T.Type, _ data: Data?, _ response: URLResponse?, completion: @escaping(_ decodedResponse: T?, _ error: String?) -> ()) {
		if let response = response as? HTTPURLResponse {
			let result = handleNetworkResponse(response)
			
			switch result {
				case .success:
					guard let responseData = data else {
						completion(nil, NetworkResponse.noData.rawValue)
						return
					}
					
					do {
						print(responseData)
						let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
						print(jsonData)
						let apiResponse = try JSONDecoder().decode(T.self, from: responseData)
						
						completion(apiResponse, nil)
					} catch {
						print(error)
						completion(nil, NetworkResponse.unableToDecode.rawValue)
					}
					
				case .failure(let networkFailureError):
					completion(nil, networkFailureError)
			}
		}
	}
    func setInteractions(_ idActivity: Int, completion: @escaping (_ response: String?, _ error: String?) -> ()) {
        let bodyParams = ["interactions": idActivity] as [String : Any]
        
        Auth.auth().currentUser?.getIDToken(completion: { (authToken, error) in
            if error != nil {
                print(error!)
                completion(nil, "There was an authentication error. Please try again.")
            } else {
                
                router.request(.setInteractions(bodyParams: bodyParams, authToken: authToken!)) { (data, response, error) in
                    if error != nil {
                        completion(nil, "Please check your network connection")
                    } else {
                    
                        self.handleResponse(ofType: GenericResponse.self, data, response) { (decodedResponse, error) in
                            completion(decodedResponse?.response, error)
                        }
                    }
                }
            }
        })
    }
}



fileprivate struct GenericResponse: Decodable {
	var response: String
}
