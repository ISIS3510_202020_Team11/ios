//
//  WeatherService.swift
//  Hobbe
//
//  Created by Valentina Duarte Benítez on 30/10/2020.
//

import Foundation
import CoreLocation
import UIKit

struct WeatherService {
	private let weatherManager = WeatherManager()
	let cache = (UIApplication.shared.delegate as! AppDelegate).weatherCache
	
	
	func getWeather(_ lat: CLLocationDegrees, _ lon: CLLocationDegrees, completion: @escaping (_ response: Double?, _ error: String?) -> ()) {
		
		/*weatherManager.getWeather(lat, lon) { (response, error) in
		completion(response, error)
		}*/
		
		/*var weather: NSNumber?*/
		
		if let cachedWeather = cache.object(forKey: "weather") {
			print("PREVIOUSLY CACHED EVENTS")
			print("\t \(cachedWeather)")
			
		}
		
		/*if let cachedVersion = cache.object(forKey: "weather") {
		weather = cachedVersion
		}*/
		
		DispatchQueue.global(qos: .background).async {
			NetworkManager.isReachable { (networkManager) in
				print("Network available")
				weatherManager.getWeather(lat, lon) { (response, error) in
					
					if let weather = response {
						let weatherNS = NSNumber.init(value: weather)
						DispatchQueue.main.async {
							cache.setObject(weatherNS, forKey: "weather")
						}
					}
					
					DispatchQueue.main.async {
						if let cachedWeather = cache.object(forKey: "weather") {
							print("CACHED WEATHER")
							print("\t \(cachedWeather)")
						}
					}
					
					completion(response, error)
					
				}
			}
			
			NetworkManager.isUnreachable { (networkManager) in
				print("Network Unavailable")
				completion(nil, "No network connection")
			}
		}
		
	}
	
	
}
