//
//  WeatherManager.swift
//  Hobbe
//
//  Created by Valentina Duarte Benítez on 30/10/2020.
//

import Foundation
import CoreLocation



protocol WeatherManagerDelegate {
    func didUpdateWeather(temp:Double)
    func didFailWithError(error: Error)
    
}


/**
Holds the methods that can be used to call the API.
*/
struct WeatherManager {
    private let router = Router<WeatherApi>()
    
    /**
    Gets the weather from the user
    - Parameters:
        - completion: Completion handler for when the API responds to the request.
    */
    func getWeather(_ lat: CLLocationDegrees, _ lon: CLLocationDegrees, completion: @escaping (_ response: Double?, _ error: String?) -> ()) {
        
        router.request(.weather(lat: lat, lon:lon)) { (data, response, error) in
            if error != nil {
                completion(nil, "Please check your network connection.")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = handleNetworkResponse(response)
                
                switch result {
                    case .success:
                        guard let responseData = data else {
                            completion(nil, NetworkResponse.noData.rawValue)
                            return
                        }
                        
                        do {
             
                            let apiResponse = try JSONDecoder().decode(Weather.self, from: responseData)
                            
                            completion(apiResponse.main.temp, nil)
                
                        } catch {
                            completion(nil, NetworkResponse.unableToDecode.rawValue)
                        }
                    case .failure(let networkFailureError):
                        completion(nil, networkFailureError)
                }
            }
        }
    }
}
