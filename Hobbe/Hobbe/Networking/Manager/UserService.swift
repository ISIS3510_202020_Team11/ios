//
//  UserManager.swift
//  Hobbe
//
//  Created by Andres Donoso on 27/10/20.
//

import UIKit

struct UserService {
    private let backendManager = BackendManager()
    var requestObject: Dictionary<String,[String]>?
    
    func saveUserInfo(name: String, lastName: String, birthdate: Date, hobbies: Hobbies, completion: @escaping (_ response: String?, _ error: String?) -> ()) {
        // Formatting date
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy"
        let formattedBirthdate = df.string(from: birthdate)
        
        // Get hobbies ids
        var hobbyIds = [Int]()
        for hobby in hobbies {
            hobbyIds.append(hobby.getId())
        }
        
        backendManager.saveUserInfo(name, lastName, formattedBirthdate, hobbyIds) { (response, error) in
            completion(response, error)
        }
    }
    
    func sendActivities(hobbies: [String], hobbiesNot: [String], completion: @escaping (_ response: String?, _ error: String?) -> ()) {
        DispatchQueue.global(qos: .background).async {
            if(NetworkManager.isConnected()) {
                backendManager.sendActivities(hobbies: hobbies, hobbiesNot: hobbiesNot) { (response, error) in
                    completion(response, error)
                }
            } else {
                completion(nil, "No network connection")
            }
        }
    }
    func setInteractions(idActivity: Int, completion: @escaping (_ response: String?, _ error: String?) -> ()) {
        
        backendManager.setInteractions(idActivity) { (response, error) in
            completion(response, error)
        }
    }
}
