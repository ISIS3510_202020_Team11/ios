//
//  RecommendationManager.swift
//  Hobbe
//
//  Created by Andres Donoso on 17/10/20.
//

import Foundation

/**
Holds the methods that can be used to call the API.
*/
struct RecommendationManager {
	private let router = Router<RecommendationApi>()
	
	/**
	Gets the recommended activities for the user.
	- Parameters:
		- completion: Completion handler for when the API responds to the request.
	*/
	func getRecommendedActivities(completion: @escaping (_ activities: [Event]?, _ error: String?) -> ()) {
		let id = Int.random(in: 1...30)
		
		router.request(.recommended(id: id)) { (data, response, error) in
			if error != nil {
				completion(nil, "Please check your network connection.")
			}
			
			if let response = response as? HTTPURLResponse {
				let result = handleNetworkResponse(response)
				
				switch result {
					case .success:
						guard let responseData = data else {
							completion(nil, NetworkResponse.noData.rawValue)
							return
						}
						
						do {
                            /*let jsonData = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                                                    print(jsonData)*/
							let apiResponse = try JSONDecoder().decode([Event].self, from: responseData)
							completion(apiResponse, nil)
						} catch {
                            print("Error aca")
							completion(nil, NetworkResponse.unableToDecode.rawValue)
						}
					case .failure(let networkFailureError):
						completion(nil, networkFailureError)
				}
			}
		}
	}
}
