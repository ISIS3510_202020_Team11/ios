//
//  NetworkResponse.swift
//  Hobbe
//
//  Created by Andres Donoso on 27/10/20.
//

import Foundation

/**
Network responses
*/
public enum NetworkResponse: String {
	case success
	case badRequest = "Bad request."
	case failed = "Network request failed."
	case noData = "Response returned with no data to decode."
	case unableToDecode = "We could not decode the response."
	case authenticationError = "You need to be authenticated first"
	case outDated = "The url requested is outdated"
}

/**
Results of the request.
*/
public enum Result<String> {
	case success
	case failure(String)
}

/**
Handles network responses given a status code.
- Parameters:
	- response: Response from the API.

- Returns: The result of the request.
*/
public func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
	switch response.statusCode {
		case 200...299: return .success
		case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
		case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
		case 600: return .failure(NetworkResponse.outDated.rawValue)
		default: return .failure(NetworkResponse.failed.rawValue)
	}
}
