//
//  BackendEndPoint.swift
//  Hobbe
//
//  Created by Andres Donoso on 27/10/20.
//

import Foundation

enum BackendApi {
    case saveUserInfo(bodyParams: Parameters, authToken: String)
    case getHobbies(authToken: String)
    case getEvents(authToken: String)
    case setInteractions(bodyParams: Parameters,authToken:String)
    case getActivitiesExplore(authToken: String)
    case sendActivities(authToken: String, hobbies: [String], hobbiesNot: [String])
}

extension BackendApi: EndPointType {
    var baseURL: URL {
        //guard let url = URL(string: "http://localhost:3000/") else { fatalError("baseURL could not be configured")}
        guard let url = URL(string: "https://hobbe-backend.herokuapp.com/") else { fatalError("baseURL could not be configured")}
        
        return url
    }
    
    var path: String {
        switch self {
            case .saveUserInfo:
                return "users/save-info"
                
            case .getHobbies:
                return "hobbies"
                
            case .getEvents:
                return "activities"
                
            case .getActivitiesExplore:
                return "activities/explore"
                
            case .setInteractions:
                return "users/interactions"
                
            case .sendActivities:
                return "users/swipe"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
            case .saveUserInfo:
                return .post
                
            case .getHobbies:
                return .get
                
            case .getEvents:
                return .get
                
            case .getActivitiesExplore:
                return .get
                
            case .setInteractions:
                return .put
                
            case .sendActivities:
                return .put
        }
    }
    
    var task: HTTPTask {
        switch self {
            case .saveUserInfo(let bodyParams, let authToken):
                return .requestParametersAndHeaders(bodyParameters: bodyParams, urlParameters: nil, additionalHeaders: ["AuthToken": authToken])
                
            case .getHobbies(let authToken):
                return .requestParametersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: ["AuthToken": authToken])
                
            case .getEvents(let authToken):
                return .requestParametersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: ["AuthToken": authToken])
                
            case .getActivitiesExplore(let authToken):
                return .requestParametersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: ["Authtoken": authToken])
                
            case .setInteractions(let bodyParams, let authToken):
                return .requestParametersAndHeaders(bodyParameters: bodyParams, urlParameters: nil, additionalHeaders: ["Authtoken": authToken])
                
            case .sendActivities(let authToken, let hobbies, let hobbiesNot):
                return .requestParametersAndHeaders(bodyParameters: ["hobbies": hobbies, "hobbiesNot": hobbiesNot], urlParameters: nil, additionalHeaders: ["Authtoken": authToken])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}
