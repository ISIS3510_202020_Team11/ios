//
//  WeatherEndPoint.swift
//  Hobbe
//
//  Created by Valentina Duarte Benítez on 30/10/2020.
//

import Foundation

public enum WeatherApi {
    case weather(lat:Double, lon: Double)
}

extension WeatherApi: EndPointType {
    var baseURL: URL {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/") else { fatalError("baseURL could not be configured") }
        
        return url
    }
    
    var path: String {
        switch self {
            case .weather:
                return "weather"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
            case .weather(let lat, let lon):
                return .requestParameters(bodyParameters: nil, urlParameters: ["appid":"e900284979164caec506449f90e1df33","units":"metric", "lat": lat, "lon": lon])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}

