//
//  RecommendationEndPoint.swift
//  Hobbe
//
//  Created by Andres Donoso on 17/10/20.
//

import Foundation

//http://willmn.pythonanywhere.com/api/recommendations?id=1

/**
End points of the API.
*/
public enum RecommendationApi {
	case recommended(id: Int)
}

extension RecommendationApi: EndPointType {
	var baseURL: URL {
		guard let url = URL(string: "http://willmn.pythonanywhere.com/api/") else { fatalError("baseURL could not be configured") }
		
		return url
	}
	
	var path: String {
		switch self {
			case .recommended:
				return "recommendations"
		}
	}
	
	var httpMethod: HTTPMethod {
		return .get
	}
	
	var task: HTTPTask {
		switch self {
			case .recommended(let id):
				return .requestParameters(bodyParameters: nil, urlParameters: ["id": id])
		}
	}
	
	var headers: HTTPHeaders? {
		return nil
	}
}
