//
//  HobbyService.swift
//  Hobbe
//
//  Created by Andres Donoso on 4/11/20.
//

import Foundation

struct HobbyService {
	private let backendManager = BackendManager()
	
	func getHobbies(completion: @escaping (_ hobbies: Hobbies?, _ error: String?) -> ()) {
		backendManager.getHobbies { (response, error) in
			completion(response, error)
		}
	}
}
