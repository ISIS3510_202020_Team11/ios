//
//  EventService.swift
//  Hobbe
//
//  Created by Valentina Duarte Benítez on 13/11/2020.
//

import Foundation
import UIKit

struct EventService {
    private let backendManager = BackendManager()
    let trendingCache = (UIApplication.shared.delegate as! AppDelegate).trendingCache
	let activitiesCache = (UIApplication.shared.delegate as! AppDelegate).activitiesCache
    
    func getTrending(completion: @escaping (_ activities: [Event]?, _ error: String?) -> ()) -> [Event]? {
        var events: [Event]?
        
        if let cachedVersion = trendingCache.object(forKey: "trending") {
            events = cachedVersion.getEvents()
        }
        
        NetworkManager.isReachable { (networkManager) in
            print("Network available")
            backendManager.getEvents { (events, error) in
                if let events = events {
                    let eventList = EventList(events: events)
                    trendingCache.setObject(eventList, forKey: "trending")
                }
                
                completion(events, error)
            }
        }
        
        NetworkManager.isUnreachable { (networkManager) in
            print("Network Unavailable")
            completion(nil, "No network connection")
        }
        
        return events
    }
	
	func getActivitiesExplore(completion: @escaping (_ cache: NSCache<NSString, EventList>?,  _ error: String?) -> ()) {
		NetworkManager.isReachable { (networkManager) in
			print("Network available")
			backendManager.getActivitiesExplore { (events, error) in
				if let events = events {
					activitiesCache.setObject(EventList(events: events), forKey: "exploreActivities")
					
					completion(activitiesCache, error)
				}
			}
		}
		
		NetworkManager.isUnreachable { (networkManager) in
			print("Network Unavailable")
			completion(nil, "No network connection")
		}
	}
}
