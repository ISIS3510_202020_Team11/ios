//
//  RecommendationService.swift
//  Hobbe
//
//  Created by Andres Donoso on 30/10/20.
//

import UIKit

struct RecommendationService {
	private let recommendationManager = RecommendationManager()
	let cache = (UIApplication.shared.delegate as! AppDelegate).recommendationsCache
	
	func getRecommendations(completion: @escaping (_ activities: [Event]?, _ error: String?) -> ()) -> [Event]? {
		var events: [Event]?
		
		if let cachedVersion = cache.object(forKey: "recommendations") {
			events = cachedVersion.getEvents()
		}
		
		NetworkManager.isReachable { (networkManager) in
			print("Network available")
			recommendationManager.getRecommendedActivities { (events, error) in
				if let events = events {
					let eventList = EventList(events: events)
					cache.setObject(eventList, forKey: "recommendations")
				}
				
				completion(events, error)
			}
		}
		
		NetworkManager.isUnreachable { (networkManager) in
			print("Network Unavailable")
			completion(nil, "No network connection")
		}
		
		return events
	}
}
