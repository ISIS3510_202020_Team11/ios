//
//  AlertPresenter.swift
//  Hobbe
//
//  Created by Andres Donoso on 27/10/20.
//

import UIKit

extension UIViewController {
	func showAlert(title: String, message: String) {
		DispatchQueue.main.async {
			let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
			
			self.present(alert, animated: true, completion: nil)
		}
	}
}
