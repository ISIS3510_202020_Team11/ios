//
//  ActivityCardView.swift
//  Hobbe
//
//  Created by Andres Donoso on 15/11/20.
//

import UIKit

class ActivityCardView: UIView {
	@IBOutlet var contentView: UIView!
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var placeLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		commonInit()
	}

	private func commonInit() {
		Bundle.main.loadNibNamed("ActivityCardView", owner: self, options: nil)
		addSubview(contentView)
		contentView.frame = self.bounds
		contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
		
		imageView.layer.cornerRadius = 10
		imageView.clipsToBounds = true
	}
}
