//
//  HorizontalProgressBar.swift
//  Hobbe
//
//  Created by Andres Donoso on 16/10/20.
//

import UIKit

@IBDesignable
class HorizontalProgressBar: UIView {
	@IBInspectable var color: UIColor = .gray {
		didSet {
			setNeedsDisplay()
		}
	}
	
	@IBInspectable var cornerRadius: CGFloat = 25 {
		didSet {
			setNeedsDisplay()
		}
	}
	
	var progress: CGFloat = 0 {
		didSet {
			setNeedsDisplay()
		}
	}
	
	private let progressLayer = CALayer()
	private let backgroundMask = CAShapeLayer()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupLayers()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setupLayers()
	}
	
	private func setupLayers() {
		layer.addSublayer(progressLayer)
	}
	
	override func draw(_ rect: CGRect) {
		backgroundMask.path = UIBezierPath(roundedRect: rect, cornerRadius: rect.height * (cornerRadius / 100)).cgPath
		layer.mask = backgroundMask
		
		let progressRect = CGRect(origin: .zero, size: CGSize(width: rect.width * progress, height: rect.height))
		
		progressLayer.frame = progressRect
		progressLayer.backgroundColor = color.cgColor
	}
}
