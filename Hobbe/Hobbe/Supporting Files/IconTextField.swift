//
//  IconTextField.swift
//  Hobbe
//
//  Created by Andres Donoso on 3/11/20.
//

import UIKit

extension UITextField {
	func setIcon(_ image: UIImage, isSquare: Bool = true) {
		let iconView = UIImageView(frame: CGRect(x: isSquare ? 15 : 10, y: 5, width: isSquare ? 20 : 30, height: 20))
		iconView.image = image
		
		let iconContainerView: UIView = UIView(frame: CGRect(x: 20, y: 0, width: 40, height: 30))
		iconContainerView.addSubview(iconView)
		
		leftView = iconContainerView
		leftViewMode = .always
	}
}
