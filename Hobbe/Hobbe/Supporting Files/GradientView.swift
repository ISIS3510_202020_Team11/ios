//
//  GradientView.swift
//  Hobbe
//
//  Created by Andres Donoso on 9/10/20.
//

import UIKit

@IBDesignable
class GradientView: UIView {
	@IBInspectable var firstColor: UIColor = UIColor.clear {
		didSet {
			updateView()
		}
	}
	
	@IBInspectable var secondColor: UIColor = UIColor.clear {
		didSet {
			updateView()
		}
	}
	
	@IBInspectable var angle: Int = 0 {
		didSet {
			updateView()
		}
	}
	
	override class var layerClass: AnyClass {
		get {
			return CAGradientLayer.self
		}
	}
	
	func updateView() {
		let layer = self.layer as! CAGradientLayer
		layer.colors = [firstColor, secondColor].map{$0.cgColor}
		
		switch angle {
			case 0...45:
				let y = (1 - (Double(angle) / 90)) * 0.5
				layer.startPoint = CGPoint(x: 0, y: y)
				layer.endPoint = CGPoint(x: 1, y: 1 - y)
				
			case 46...135:
				let x = (Double(angle) - 45) / 90
				layer.startPoint = CGPoint(x: x, y: 0)
				layer.endPoint = CGPoint(x: 1 - x, y: 1)
				
			case 136...180:
				let y = (Double(angle) - 135) / 90
				layer.startPoint = CGPoint(x: 1, y: y)
				layer.endPoint = CGPoint(x: 0, y: 1 - y)
				
			default:
				layer.startPoint = CGPoint(x: 0.5, y: 0)
				layer.endPoint = CGPoint(x: 0.5, y: 1)
		}
	}
}
