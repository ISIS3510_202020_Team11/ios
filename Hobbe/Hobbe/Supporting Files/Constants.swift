//
//  Constants.swift
//  Hobbe
//
//  Created by Andres Donoso on 3/11/20.
//

import Foundation

struct Constants {
	struct Storyboard {
		//MARK: - Controllers
		static let homeViewController = "HomeVC"
		static let selectHobbiesViewController = "SelectHobbiesVC"
		static let tabBarController = "TabBarController"
		static let welcomeNVC = "welcomeNVC"
		
		//MARK: - Segues
		static let signUpSegue = "signUpSegue"
		static let hobbiesTableSegue = "hobbiesTableSegue"
		
		//MARK: - Tables
		static let hobbyCellIdentifier = "HobbyTableViewCell"
	}
}
