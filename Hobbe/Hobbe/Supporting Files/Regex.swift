//
//  Regex.swift
//  Hobbe
//
//  Created by Andres Donoso on 17/10/20.
//

import Foundation

extension String {
	
	/**
	Checks if the string is formatted as the regex on the right side.
	
	- Parameters:
		- lhs: String to be checked
		- rhs: Regex used
	
	- Returns: Whether the string is formatted as specified by the regex.
	*/
	static func ~= (lhs: String, rhs: String) -> Bool {
		guard let regex = try? NSRegularExpression(pattern: rhs) else { return false }
		let range = NSRange(location: 0, length: lhs.utf16.count)
		
		return regex.firstMatch(in: lhs, options: [], range: range) != nil
	}
}
