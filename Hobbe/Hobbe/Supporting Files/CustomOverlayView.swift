//
//  OverlayView.swift
//  Hobbe
//
//  Created by Andres Donoso on 15/11/20.
//

import UIKit
import Koloda

private let overlayRightImageName = "overlay_like"
private let overlayleftImageName = "overlay_skip"

class CustomOverlayView: OverlayView {
	@IBOutlet lazy var overlayImageView: UIImageView! = { [unowned self] in
		var imageView = UIImageView(frame: self.bounds)
		self.addSubview(imageView)
		
		return imageView
	}()
	
	override var overlayState: SwipeResultDirection? {
		didSet {
			switch overlayState {
				case .left:
					overlayImageView.image = UIImage(named: overlayleftImageName)
					break
					
				case .right:
					overlayImageView.image = UIImage(named: overlayRightImageName)
					break
					
				default:
					overlayImageView.image = nil
			}
		}
	}
}
